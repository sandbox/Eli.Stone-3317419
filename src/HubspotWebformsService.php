<?php

namespace Drupal\hubspot_webforms;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\hubspot_api\Manager;

/**
 * Hubspot webforms service.
 */
class HubspotWebformsService {

  /**
   * The config for hubspot_webforms.settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The hubspot api manager.
   *
   * @var \Drupal\hubspot_api\Manager
   */
  protected Manager $manager;

  /**
   * Hubspot handler.
   *
   * @var \SevenShores\Hubspot\Factory|null
   */
  protected $hubspotHandler;

  /**
   * Construct the hubspot webform service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\hubspot_api\Manager $manager
   *   Connection to the hubspot_api manager.
   *
   * @throws \Exception
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, Manager $manager) {
    $this->config = $config_factory->get('hubspot_webforms.settings');
    $this->logger = $logger_factory->get('hubspot_webforms');
    $this->manager = $manager;
    $this->hubspotHandler = $manager->getHandler();
  }

  /**
   * Checks if hubspot is connected.
   */
  public function hubspotConnected(): bool {
    $cacheId = "hubspot_webforms_connection_state";
    // We don't need to check the connection every time.
    if ($cache = \Drupal::cache()->get($cacheId)) {
      $state = $cache->data;
    }
    else {
      $state = $this->manager->testApi();
      if ($state) {
        \Drupal::cache()->set($cacheId, $state, Cache::PERMANENT);
      }
    }
    return $state;
  }

  /**
   * Gets hubspot forms.
   *
   * @param bool $sorting
   *   Enable/disable sorting.
   */
  public function getHubspotForms(bool $sorting = TRUE): array {
    $cacheId = "hubspot_webforms_form_data";

    // Load from cache first next get from hubspot.
    if ($cache = \Drupal::cache()->get($cacheId)) {
      $hubspot_forms = $cache->data;
    }
    else {
      $hubspot_forms = $this->hubspotHandler->forms()->all()->getData();
      if ($hubspot_forms) {
        \Drupal::cache()->set($cacheId, $hubspot_forms, Cache::PERMANENT);
      }
    }

    if ($sorting) {
      $hubspot_forms = array_column($hubspot_forms, NULL, 'guid');
      $options = array_column($hubspot_forms, 'name', 'guid');
      asort($options, SORT_STRING | SORT_FLAG_CASE);
      return $options;
    }

    return $hubspot_forms;
  }

  /**
   * Returns all the form fields.
   *
   * @param string $formGuid
   *   Form GUID.
   */
  public function getHubspotFormFields(string $formGuid) {
    $forms = $this->getHubspotForms(FALSE);
    $form = array_column($forms, NULL, 'guid')[$formGuid] ?? NULL;
    if ($form) {
      return $form->formFieldGroups;
    }
    return NULL;
  }

  /**
   * Submits Form data.
   */
  public function submitFormData(string $formGuid, $fields, $legalConsentOptions, $context = []) {
    $portalId = $this->config->get('portal_id');
    $hutk = $this->config->get('hutk');
    $data = [];

    if (!$portalId) {
      $this->logger->error("Missing 'portal_id' to submit form. Check config.");
      return;
    }

    $data['fields'] = $fields;
    $data['context'] = $context;

    if ($legalConsentOptions) {
      $data['legalConsentOptions'] = $legalConsentOptions;
    }

    if ($hutk) {
      $data['context']['hutk'] = $hutk;
    }

    $this->hubspotHandler->forms()->submit($portalId, $formGuid, $data);
  }

}

<?php

namespace Drupal\hubspot_webforms\Plugin\WebformHandler;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hubspot_webforms\HubspotWebformsService;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create a new node entity from a webform submission.
 *
 * @WebformHandler(
 *   id = "hubspot_webforms_submit",
 *   label = @Translation("Submit to hubspot"),
 *   category = @Translation("hubspot"),
 *   description = @Translation("Submits data to hubspot"),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class HubspotWebformsSubmitWebformHandler extends WebformHandlerBase {
  /**
   * Fields that we don't want as select options.
   */
  const EXCLUDE_FIELD_ELEMENTS = [
    'webform_actions',
    'webform_flexbox',
    'webform_markup',
    'webform_more',
    'webform_section',
    'webform_wizard_page',
    'webform_message',
    'webform_horizontal_rule',
    'webform_terms_of_service',
    'webform_computed_token',
    'webform_computed_twig',
    'webform_element',
    'processed_text',
    'captcha',
    'container',
    'details',
    'fieldset',
    'item',
    'label',
  ];

  /**
   * Fields we want for consent options.
   */
  const CONSENT_FIELD_TYPES = [
    'checkbox',
    'webform_terms_of_service',
  ];

  /**
   * Internal reference to the hubspot forms.
   *
   * @var \Drupal\hubspot_webforms\HubspotWebformsService
   */
  protected HubspotWebformsService $hubspotWebformsService;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id,
      $plugin_definition);
    $instance->hubspotWebformsService = $container->get('hubspot_webforms.service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    if (!$this->hubspotWebformsService->hubspotConnected()) {
      $form['status_report'] = [
        '#markup' => "<span>" . $this->t('Hubspot is not connected, please check your settings.') . "</span>",
      ];
      return $form;
    }
    $settings = $this->getSettings();
    $defaultHubspotGuid = $settings['form_guid'] ?? NULL;

    $form['mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Mapping'),
    ];

    $hubspotForms = $this->hubspotWebformsService->getHubspotForms();

    // Select list of forms on hubspot.
    $form['mapping']['hubspot_form'] = [
      '#type' => 'select',
      '#title' => $this->t('Hubspot form:'),
      '#options' => $hubspotForms,
      '#default_value' => $defaultHubspotGuid,
      '#empty_option' => $this->t('Select a form'),
      '#ajax' => [
        'callback' => [$this, 'getHubspotFormFieldsAjaxCallback'],
        'event' => 'change',
        'wrapper' => 'edit-settings-mapping-field-group-fields-wrapper',
      ],
    ];

    // Fieldset to contain mapping fields.
    $form['mapping']['field_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields to map for form: @label',
        ['@label' => $this->webform->label()]),
      '#states' => [
        'invisible' => [
          ':input[name="settings[mapping][hubspot_form]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['mapping']['field_group']['fields'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-settings-mapping-field-group-fields-wrapper">',
      '#suffix' => '</div>',
      '#markup' => '',
    ];

    $form_values = $form_state->getValues();
    if (!empty($form_values['mapping']['hubspot_form']) || !empty($defaultHubspotGuid)) {
      $hubspot_guid = $form_values['mapping']['hubspot_form'] ?? $defaultHubspotGuid;
      $hubspotFormsFields = $this->hubspotWebformsService->getHubspotFormFields($hubspot_guid);

      // Creates the field mapping list.
      if ($hubspotFormsFields) {
        $webformFieldOptions = $this->getWebformFieldOptions();
        $defaultFieldOptions = $webformFieldOptions['default'] ?? [];
        $consentFieldOptions = $webformFieldOptions['consent'] ?? [];

        foreach ($hubspotFormsFields as $fieldGroup) {
          foreach ($fieldGroup->fields as $field) {
            $form['mapping']['field_group']['fields'][$field->name] = [
              '#title' => $field->label . ' (' . $field->fieldType . ')',
              '#type' => 'select',
              '#options' => $defaultFieldOptions,
            ];
            if (isset($settings['field_mapping'][$field->name])) {
              $form['mapping']['field_group']['fields'][$field->name]['#default_value'] = $settings['field_mapping'][$field->name];
            }
          }
        }

        $form['legal_consent'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Legal consent'),
        ];
        $form['legal_consent']['status'] = [
          '#type' => 'select',
          '#title' => $this->t('Include Legal Consent status'),
          '#options' => [
            'never' => $this->t('Never'),
            'always' => $this->t('Always'),
            'conditionally' => $this->t('Conditionally'),
          ],
          '#default_value' => $settings['legal_consent']['status'] ?? 'never',
        ];
        $form['legal_consent']['source'] = [
          '#title' => $this->t('Source Value'),
          '#type' => 'select',
          '#options' => $consentFieldOptions,
          '#default_value' => $settings['legal_consent']['source'] ?? 'never',
          '#description' => $this->t('If no options given make sure the webform has at least one of the following field types: @types', [
            '@types' => implode(",\n\r", self::CONSENT_FIELD_TYPES),
          ]),
          '#states' => [
            'visible' => [
              ':input[name="settings[legal_consent][status]"]' => [
                'value' => 'conditionally',
              ],
            ],
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * AJAX callback for hubspot form change event.
   *
   * @param array $form
   *   Active form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Active form state.
   *
   * @return array
   *   Render array.
   */
  public function getHubspotFormFieldsAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['settings']['mapping']['field_group']['fields'];
  }

  /**
   * Get Webform field Options.
   */
  public function getWebformFieldOptions(): array {
    $components = $this->webform->getElementsInitializedAndFlattened();
    $webformFieldsOptions = [
      '' => '- Do Not Map -',
    ];
    $webformConsentFieldsOptions = [];

    foreach ($components as $webform_field => $value) {
      if (!in_array($value['#type'], static::EXCLUDE_FIELD_ELEMENTS)) {
        if ($value['#webform_composite']) {
          foreach ($value['#webform_composite_elements'] as $composite_field => $composite_value) {
            $key = $webform_field . ':' . $composite_field;
            $webformFieldsOptions[$key] = (@$value['#title'] . ': ' . $composite_value['#title'] ?: $key) . ' (' . $composite_value['#type'] . ')';
          }
        }
        else {
          $webformFieldsOptions[$webform_field] = (@$value['#title'] ?: $webform_field) . ' (' . $value['#type'] . ')';

          if (in_array($value['#type'], self::CONSENT_FIELD_TYPES)) {
            $webformConsentFieldsOptions[$webform_field] = (@$value['#title'] ?: $webform_field) . ' (' . $value['#type'] . ')';
          }
        }
      }
    }

    if (empty($webformConsentFieldsOptions)) {
      $webformConsentFieldsOptions[''] = '- No options found -';
    }

    return [
      'default' => $webformFieldsOptions,
      'consent' => $webformConsentFieldsOptions,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    if (!$this->hubspotWebformsService->hubspotConnected()) {
      return;
    }

    $form_values = $form_state->getValues();

    $hubspot_id = $form_values['mapping']['hubspot_form'];
    $fields = $form_values['mapping']['field_group']['fields'];

    $settings = [];

    if ($hubspot_id != '') {
      $settings['form_guid'] = $hubspot_id;
      $settings['field_mapping'] = array_filter($fields,
        function ($hubspot_field) {
          return $hubspot_field !== '';
        });
      $this->messenger()->addMessage($this->t('Saved new field mapping.'));
    }

    $settings['legal_consent'] = $form_values['legal_consent'];
    if ($settings['legal_consent']['status'] != 'conditionally') {
      unset($settings['legal_consent']['source']);
    }

    $this->setSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $settings = $this->getSettings();
    $webform = $this->getWebform();
    $form_guid = $settings['form_guid'];
    $field_mapping = $settings['field_mapping'];
    $webform_values = $webform_submission->getData();
    $elements = $webform->getElementsDecodedAndFlattened();

    $form_values = $this->createMappingDataToSend($field_mapping, $webform_values);
    $legalConsent = $this->createLegalConsent($settings, $elements, $webform_values);

    $context = [
      "pageUri" => $webform->toUrl('canonical', ['absolute' => TRUE])->toString() ?? "",
      "pageName" => $webform->get('title'),
      'ipAddress' => \Drupal::request()->getClientIp(),
    ];

    $this->hubspotWebformsService->submitFormData($form_guid, $form_values, $legalConsent, $context);
  }

  /**
   * Creates mapping data to send to form.
   */
  public function createMappingDataToSend($fieldMapping, $webformValues): array {
    $output = [];
    foreach ($fieldMapping as $hubspotField => $webformPath) {
      if ($webformPath != '') {
        if (strpos($webformPath, ':') !== FALSE) {
          // Is composite element.
          $composite = explode(':', $webformPath);
          $composite_value = NestedArray::getValue($webformValues, $composite);
          $output[] = [
            "name" => $hubspotField,
            "value" => $composite_value,
          ];
        }
        else {
          // Not a composite element.
          $output[] = [
            "name" => $hubspotField,
            "value" => $webformValues[$webformPath],
          ];
        }
      }
    }
    return $output;
  }

  /**
   * Creates legal consent data to send to form.
   */
  public function createLegalConsent($settings, $elements, $webformValues): array {
    $output = [];
    $approved = FALSE;

    if (isset($settings['legal_consent']) && $settings['legal_consent']['status'] != 'never') {
      $text = $this->t('I agree');

      // If always is enabled.
      if ($settings['legal_consent']['status'] == 'always') {
        $approved = TRUE;
      }

      // If conditionally we want to load up the field and check its status.
      if ($settings['legal_consent']['status'] == 'conditionally' && isset($settings['legal_consent']['source'])) {
        if (in_array($elements[$settings['legal_consent']['source']]['#type'], self::CONSENT_FIELD_TYPES)) {
          if ($webformValues[$settings['legal_consent']['source']] == 1) {
            $approved = TRUE;
            $text = $elements[$settings['legal_consent']['source']]['#title'];
          }
        }
      }

      // If approved create the consent array.
      if ($approved) {
        $output['consent']['consentToProcess'] = TRUE;
        $output['consent']['text'] = $text;
      }
    }
    return $output;
  }

}

<?php

namespace Drupal\hubspot_webforms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class HubspotWebformSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'hubspot_webforms.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_webforms_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['portal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Portal ID / Hub ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('portal_id'),
      '#description' => $this->t('How to find your <a href="@url">Hub ID</a>', [
        '@url' => 'https://knowledge.hubspot.com/account/manage-multiple-hubspot-accounts',
      ]),
    ];

    $form['hutk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hubspot Tracking Key'),
      '#default_value' => $config->get('hutk'),
      '#description' => implode("<br>", [
        $this->t("The tracking cookie token value used for HubSpot lead activity tracking."),
        $this->t("You can retrieve this value from the 'Hubspotutk' cookie placed in the user's browser by the HubSpot JavaScript Tracking Code; the tracking code must be installed on the page that contains the form."),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      ->set('portal_id', $form_state->getValue('portal_id'))
      ->set('hutk', $form_state->getValue('hutk'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
